#License applicable to the recipe file only,  not to the packages installed by this recipe.
LICENSE = "BSD-3-Clause-Clear"

inherit psdk-base psdk-package psdk-pickup

SRC_URI = "file://${@d.getVar('CONFIG_OSS_SELECT')}"
SRC_URI =+ "file://script_oss"

# The path infos of qirp content
SAMPLES_PATH = "${TOPDIR}/workspace/src/vendor/qcom/opensource/qirp-oss"
TOOLCHAIN_PATH = "${TOPDIR}/SDK"
SETUP_PATH = "${FILE_DIRNAME}/files/setup.sh"

# The name and version of qirp SDK artifact
SDK_PN = "qirp_sdk"
PV = "1.0.0"

# The functionality of qirp SDK
# DEPENDS += "qti-qim"
# DEPENDS += "qti-robotics"

#!/bin/bash

export SDK_TOP_DIR=$PWD

#create toolchain dir
if [ ! -d "$SDK_TOP_DIR/toolchain/install_dir" ];then
  # Install toolchain
  search_dir="toolchain"
  for file in $search_dir/*.sh; do
    echo $file
      mkdir toolchain/install_dir
      ./"$file" -d $PWD/toolchain/install_dir -y
  done

  #tar -zxvf qirp tar.gz
  search_dir="runtime"
  cd $search_dir
  for file in *tar.gz; do
    echo $file
    tar -zxvf "$file"
  done

  #install qirp packages
  for file in product*; do
    echo $file
    if [ -d "$file" ]; then
      cd $file
      ar -x qirp-*
      xz -d data.tar.xz && tar -xf data.tar -C $SDK_TOP_DIR/toolchain/install_dir/sysroots/*-oe-linux
      cd ../
    fi
  done
cd $SDK_TOP_DIR
fi

#install or uninstall qirp
if [ "$1" == "uninstall" ]; then
  if [ -d "toolchain/install_dir" ]; then
    rm -rf toolchain/install_dir
    rm -rf runtime/product*/*
  fi
  echo "uninstall qirp sdk "
else
  export search_dir=$SDK_TOP_DIR/toolchain/install_dir/environment*linux
  for file in $search_dir;do
    . "$file"
  done
  echo "setup qirp sysroot done!"
fi

inherit psdk-extract

#License applicable to the recipe file only,  not to the packages installed by this recipe.
LICENSE = "BSD-3-Clause-Clear"

# the information of function sdk package(s)
CONFIGFILE = "${@d.getVar('CONFIG_OSS_SELECT')}"
SDKSPATH = "${TOPDIR}/function_sdk/qirf_sdk/robotics_function_sdk_oss.tar.gz"

FILES_SKIP = "${D}/${PN}/packages_oss \
              ${D}/${PN}/pathplan \
              "
do_fetch_extra[depends] += "${@bb.utils.contains('BUILD_QIRF_SDK_SOURCE', 'True', 'robotics-oss-populate:do_populate_artifacts', '', d)}"

inherit psdk-extract

# License applies to this recipe code, not to the packages installed by this recipe
LICENSE = "BSD-3-clause-clear"

# the information of function sdk package(s)
CONFIGFILE = "${@d.getVar('CONFIG_OSS_SELECT')}"
SDKSPATH = "${TOPDIR}/function_sdk/qim_sdk/packages.zip"

FILES_SKIP = "${D}/${PN}/*gst*"

